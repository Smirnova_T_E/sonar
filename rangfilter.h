#ifndef RANGFILTER_H
#define RANGFILTER_H

#include <QWidget>
#include <QtCharts>
#include <QBarSet>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QGraphicsSceneEvent>
#include <QGraphicsEllipseItem>

using namespace std;

namespace Ui {
class RangFilter;
}

class RangFilter : public QWidget
{
    Q_OBJECT

public:
    explicit RangFilter(QWidget *parent = 0);
    ~RangFilter();
    QChartView * chartView;
    QBarSeries *barSeries;
    QChart *chart;
    QBarCategoryAxis *xAxis;
    QValueAxis *yAxis;
    QHBoxLayout *hlay;
    QBarSet *set0;
    QBarSet *set1;
    QBarSet *set2;
    QBarSet *set3;

private:
    Ui::RangFilter *ui;
signals:

public slots:
  //  void setXY(vector <int> v, vector <int> v_3, vector <int> v_5, vector <int> v_7);
    void setRF(vector <int> v, int index);
    void setRF_G3(vector <int> v, int index);
    void setRF_G5(vector <int> v, int index);
    void setRF_G7(vector <int> v, int index);
    void scale (int index, int slid);
    void clear();
    void clearGr();
    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
};

#endif // RANGFILTER_H
