#ifndef HOUGHLINE_H
#define HOUGHLINE_H

#include <QWidget>
#include <opencv2/opencv.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

namespace Ui {
class HoughLine;
}

class HoughLine : public QWidget
{
    Q_OBJECT

public:
    explicit HoughLine(QWidget *parent = 0);
    ~HoughLine();
    cv::Mat image_HL;
    cv::Mat imageMid_HL;

private:
    Ui::HoughLine *ui;

signals:
    void sent_on_label_HL (cv::Mat img);
    void setGraphic(int y);
    void sent_Data(int a, int b, int c, int d, int e);

private slots:
    void setHoughLine();
    void getFrame_HL(cv::Mat img);
    void setGraph ();
    void sentData();
};



#endif // HOUGHLINE_H
