#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <QWidget>
#include <QtCharts>

namespace Ui {
class Analysis;
}

class Analysis : public QWidget
{
    Q_OBJECT

public:
    explicit Analysis(QWidget *parent = 0);
    ~Analysis();
    QChartView * chartView;
    QScatterSeries *scatterSeries;
    QChart *chart;
    QValueAxis *xAxis;
    QValueAxis *yAxis;
    QHBoxLayout *hlay;
    int x_mean;

private:
    Ui::Analysis *ui;

public slots:
    void scale (int index, int slid);
    void clear();
    void setXY(int y);

    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
    void clearGr();
};

#endif // ANALYSIS_H
