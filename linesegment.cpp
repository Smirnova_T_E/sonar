#include "linesegment.h"
#include "mainwindow.h"
#include "ui_linesegment.h"
#include "ui_fastline.h"
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>
#include <QDebug>

LineSegment::LineSegment(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LineSegment)
{
    image_LS.release();
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(setLineSegment()));
    connect(ui->pushButton_2, SIGNAL(clicked(bool)), this, SLOT(setGraph()));
    connect(ui->pushButton_3, SIGNAL(clicked(bool)), this, SLOT(sentData()));
}

LineSegment::~LineSegment()
{
    delete ui;
}

void LineSegment::setLineSegment()
{
    int a = ui->spinBox->value();
    int b = ui->spinBox_2->value();
    int c = ui->spinBox_3->value();
    cv::Canny(image_LS, imageMid_LS, b, a, c);
    cv::Ptr <cv::LineSegmentDetector> ls = cv::createLineSegmentDetector();
    std::vector<cv::Vec4i> lines_std;
    ls->detect(imageMid_LS, lines_std);
    std::vector<cv::Vec4i>::const_iterator abc = lines_std.begin();
    int k = 0;
    while (abc!=lines_std.end())

     {
       k=k+1;
       abc++;
     }
    qDebug() << k << "before";
    imageMid_LS = image_LS;
    ls->drawSegments(imageMid_LS, lines_std);
    emit sent_on_label_LS(imageMid_LS);
    update();

}

void LineSegment::getFrame_LS(cv::Mat img)
{
    image_LS = img;

}

void LineSegment::setGraph()
{
    int yMean = ui->spinBox_4->value();
    emit setGraphic(yMean);
}

void LineSegment::sentData()
{
    int a = ui->spinBox->value();
    int b = ui->spinBox_2->value();
    int c = ui->spinBox_3->value();
    int d = ui->spinBox_4->value();
    emit sent_Data(a, b, c, d, 0);
}
