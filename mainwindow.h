#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ui_mainwindow.h>
#include <QWidget>
#include <QPixmap>
#include <QImage>
#include <QDebug>
#include <opencv2/opencv.hpp>
#include <QMouseEvent>
#include <cmath>
#include <math.h>
using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    cv::Mat frame;
    cv::Mat image;
    cv::Mat image_1;
    cv::Mat image_2;
    cv::Mat image_3;
    cv::Mat image_rangFilter;
    cv::Mat image_LineSegment;
    int slider;
    vector <int> lines, lines_3, lines_5,lines_7;
    void mousePressEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *e);


private:
    Ui::MainWindow *ui;



signals:
    void setPanelMode (QWidget *wgt);
    void sentFrame(cv::Mat img);
    void setInitialFrame (cv::Mat img);
    void sentFrame_LS(cv::Mat img);
    void sentFrame_HL(cv::Mat img);
    void sentFrame_HC(cv::Mat img);

    void setGraph (QWidget *wgt);
    void graph_rangFilter ();
    void graph_LineSegment ();
 //   void setXY (vector <int> v, vector <int> v_3, vector <int> v_5, vector <int> v_7);
 //   void setXY_LS (vector <int> v, vector <int> v_3, vector <int> v_5, vector <int> v_7);
    void set0 (int index, int slid);
    void set1 (int index, int slid);
    void set2 (int index, int slid);
    void set3 (int index, int slid);
    void clear0();
    void clear1();
    void clear2();
    void clear3();

    void RF0 (vector <int> v, int index);
    void RF1 (vector <int> v, int index);
    void RF2 (vector <int> v, int index);
    void RF3 (vector <int> v, int index);

    void LS0 (vector <int> v, int index);
    void LS1 (vector <int> v, int index);
    void LS2 (vector <int> v, int index);
    void LS3 (vector <int> v, int index);
    void clearGr();
    void clearGr2();

    void left_RF();
    void right_RF();
    void up_RF();
    void down_RF ();

    void left_two();
    void right_two();
    void up_two();
    void down_two ();
    void set_Graphmethod (QWidget *wgt);

    void left_An();
    void right_An();
    void up_An();
    void down_An ();

private slots:
    void SearchMethod (int index);
    void setFilter(bool checked);
    void setFilter_2(bool checked);
    void setImage(int index);
    void setDilate (bool checked);
    void setRangFilter();
    void setGaussFilter(int index);
    void set_on_label (cv::Mat img);
    void set_on_label_HL (cv::Mat img);
    void set_Initial_Frame(cv::Mat img);
    void setDilateOnFrame (int index);

    void setGraphic (int index);
    void graphRangFilter ();
    void graphLineSegment2 ();
    void setScale (int index);
    void slider_int ();
    void clearScale();
    void setRF(bool checked);
    void setRF_G3(bool checked);
    void setRF_G5(bool checked);
    void setRF_G7(bool checked);

    void setGraphMethod ();
    void writeFile(int a, int b, int c, int d, int e);
};

#endif // MAINWINDOW_H
