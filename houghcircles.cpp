#include "houghcircles.h"
#include "ui_houghcircles.h"
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>

HoughCircles::HoughCircles(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HoughCircles)
{
    image_HC.release();
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(setHoughCircles()));
    connect(ui->pushButton_2, SIGNAL(clicked(bool)), this, SLOT(setGraph()));
}

class CirclesFinder
{
public:
    cv::Mat img;
    double resolution;
    double minDist;
    int minVote;
    double highThersh;
    double minRad;
    double maxRad;
    std::vector<cv::Vec3f> circles;
    int k =0;

    void setAccResolution (double accRes)
        {
            resolution = accRes;
        }

    void setMinVote (int minv){
            minVote = minv;
        }

    void setMinDist (double length){
            minDist = length;
        }

    void setCannyThresh (double thresh){
            highThersh = thresh;
        }

    void setRadius (double radius_1, double radius_2){
            minRad = radius_1;
            maxRad = radius_2;
        }

    std::vector<cv::Vec3f> findCircles (cv::Mat& binary, int c){
            circles.clear();

               do
                {
                  k=0;
                  minVote=minVote+10;
                  cv::HoughCircles(binary, circles, CV_HOUGH_GRADIENT, resolution, minDist, highThersh, minVote, minRad, maxRad);
                  std::vector<cv::Vec3f>::const_iterator abc = circles.begin();
                  while (abc!=circles.end())

                   {
                     k=k+1;
                     abc++;
                   }
                }
                while (k > c);
            return circles;
        }

    void drawDetectedLines (cv::Mat& image, cv::Scalar color){
            color = cv::Scalar(255,255,255);
            std::vector<cv::Vec3f>::const_iterator it2 = circles.begin();
            while (it2!=circles.end())
            {
              cv::circle (image, cv::Point((*it2)[0], (*it2)[1]), (*it2)[2], color, 2);
              ++it2;
            }
        }

};

HoughCircles::~HoughCircles()
{
    delete ui;
}

void HoughCircles::setHoughCircles()
{
    int a = ui->spinBox_3->value();
    int b = ui->spinBox_4->value();
    int d = ui->spinBox_2->value();
    imageMid_HC.release();
    imageMid_HC = image_HC;
    CirclesFinder finder_Circles;
    finder_Circles.setAccResolution(2);
    finder_Circles.setMinVote(0);
    finder_Circles.setMinDist(100);
    finder_Circles.setCannyThresh(d);
    finder_Circles.setRadius(b, a);
    int c = ui->spinBox->value();
    std::vector<cv::Vec3f> circles = finder_Circles.findCircles(imageMid_HC, c);
    imageMid_HC = image_HC;
    finder_Circles.drawDetectedLines(imageMid_HC, cv::Scalar(255,255,255));
    emit sent_on_label_HC(imageMid_HC);
    update();
}

void HoughCircles::getFrame_HC(cv::Mat img)
{
    image_HC = img;
}

void HoughCircles::setGraph()
{
    int yMean = ui->spinBox_5->value();
    emit setGraphic(yMean);
}

void HoughCircles::sentData()
{
    int a = ui->spinBox_3->value();
    int b = ui->spinBox_4->value();
    int c = ui->spinBox->value();
    int d = ui->spinBox_2->value();
    int e = ui->spinBox_5->value();
    emit sent_Data(a, b, c, d, e);
}
