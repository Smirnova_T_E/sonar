#include "fastline.h"
#include "mainwindow.h"
#include "ui_fastline.h"
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>
#include <math.h>
#include <QDebug>

FastLine::FastLine(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FastLine)
{
    image_fastLine.release();
    ui->setupUi(this);
    connect (ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(setFastLine()));
    connect(ui->pushButton_3, SIGNAL(clicked(bool)), this, SLOT(setGraph()));
    connect(ui->pushButton_2, SIGNAL(clicked(bool)), this, SLOT(sentData()));

}

FastLine::~FastLine()
{
    delete ui;
}

void FastLine::setFastLine()
{
    imageMid = image_fastLine.clone();
    int a = ui->spinBox_4->value();
    int b = ui->spinBox->value();
    int c = ui->spinBox_2->value();
    int d = ui->spinBox_3->value();
    cv::Ptr <cv::ximgproc::FastLineDetector> fastLine = cv::ximgproc::createFastLineDetector(a, 3.0, b, c, d, true);
    std::vector <cv::Vec4f> lines_fld;
    fastLine->detect(image_fastLine, lines_fld);
    std::vector<cv::Vec4f>::const_iterator abc = lines_fld.begin();
    int k = 0;
    while (abc!=lines_fld.end())

     {
       k=k+1;
       abc++;
     }

    qDebug() << k << "before";
    fastLine->drawSegments(imageMid, lines_fld);

    emit sent_on_label(imageMid);
    update();
}

void FastLine::getFrame(cv::Mat img)
{
    image_fastLine = img;

}

void FastLine::setGraph()
{
    int yMean = ui->spinBox_5->value();
    emit setGraphic(yMean);
}

void FastLine::sentData()
{
    int a = ui->spinBox_4->value();
    int b = ui->spinBox->value();
    int c = ui->spinBox_2->value();
    int d = ui->spinBox_3->value();
    int e = ui->spinBox_5->value();
    emit sent_Data(a, b, c, d, e);
}


