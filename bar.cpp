#include "bar.h"
#include "ui_bar.h"

Bar::Bar(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Bar)
{
    ui->setupUi(this);
    chartView = new QChartView();
    barSeries = new QBarSeries();
    chart = new QChart();
    xAxis = new QBarCategoryAxis;
    yAxis = new QValueAxis;

    hlay = new QHBoxLayout();
    hlay->addWidget(chartView);
    setLayout(hlay);

    QStringList categories;
    categories << "0" << "3" << "6" << "9" << "12" << "15" << "18" << "21" << "24";

    chartView->chart()->addSeries(barSeries);
    xAxis->append(categories);
    chartView->chart()->createDefaultAxes();
    chartView->chart()->setAxisX(xAxis, barSeries);
    chartView->chart()->setAxisY(yAxis, barSeries);
    chartView->setRenderHint(QPainter::Antialiasing);

    yAxis->setRange(0, 100);
    yAxis->setTickCount(11);
    yAxis->setTitleText("Кол-во линий");
    xAxis->setTitleText("№ пикселя фильтра");


}

Bar::~Bar()
{
    delete ui;
}

void Bar::scale(int index, int slid)
{

}

void Bar::clear()
{
    chartView->chart()->zoomReset();

}
