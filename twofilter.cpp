#include "twofilter.h"
#include "ui_twofilter.h"
using namespace std;

TwoFilter::TwoFilter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TwoFilter)
{

    ui->setupUi(this);
    chartView = new QChartView();
    barSeries = new QBarSeries();
    chart = new QChart();
    xAxis = new QBarCategoryAxis();
    yAxis = new QValueAxis;

    hlay = new QHBoxLayout();
    hlay->addWidget(chartView);
    setLayout(hlay);

    set0_LS = new QBarSet ("RF");
    set1_LS = new QBarSet ("RF+Gauss 3x3");
    set2_LS = new QBarSet ("RF+Gauss 5x5");
    set3_LS = new QBarSet ("RF+Gauss 7x7");

    chartView->chart()->addSeries(barSeries);
    chartView->chart()->setAnimationOptions(QChart::SeriesAnimations);

    QStringList categories;
    categories << "0" << "3" << "6" << "9" << "12" << "15" << "18" << "21" << "24";

    xAxis->append(categories);
    chartView->chart()->createDefaultAxes();
    chartView->chart()->setAxisX(xAxis, barSeries);
    chartView->chart()->setAxisY(yAxis, barSeries);
    chartView->chart()->legend()->setVisible(true);
    chartView->setRenderHint(QPainter::Antialiasing);

    yAxis->setRange(100, 1000);
    yAxis->setTickCount(10);
    yAxis->setTitleText("Кол-во линий");
    xAxis->setTitleText("№ пикселя фильтра");
}

TwoFilter::~TwoFilter()
{
    delete ui;
}

void TwoFilter::setRF(vector<int> v, int index)
{
    set0_LS->remove(0,set0_LS->count());
    if (index == 1) {
        *set0_LS << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set0_LS);
    }
    else {
        *set0_LS << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set0_LS);
    }
    update();
}

void TwoFilter::setRF_G3(vector<int> v, int index)
{
    set1_LS->remove(0,set1_LS->count());
    if (index == 1) {
        *set1_LS << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set1_LS);
    }
    else {
        *set1_LS << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set1_LS);
    }
    update();

}

void TwoFilter::setRF_G5(vector<int> v, int index)
{
    set2_LS->remove(0,set2_LS->count());
    if (index == 1) {
        *set2_LS << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set2_LS);
    }
    else {
        *set2_LS << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set2_LS);
    }
    update();

}

void TwoFilter::setRF_G7(vector<int> v, int index)
{
    set3_LS->remove(0,set3_LS->count());
    if (index == 1) {
        *set3_LS << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set3_LS);
    }
    else {
        *set3_LS << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set3_LS);
    }
    update();

}

//void TwoFilter::setXY(vector<int> v, vector<int> v_3, vector<int> v_5, vector<int> v_7)
//{
//    set0_LS->remove(0,set0_LS->count());
//    set1_LS->remove(0,set1_LS->count());
//    set2_LS->remove(0,set2_LS->count());
//    set3_LS->remove(0,set3_LS->count());

//  *set0_LS << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
//  barSeries->append(set0_LS);
//  *set1_LS << v_3[0] << v_3[1]<< v_3[2]<< v_3[3]<< v_3[4]<< v_3[5]<< v_3[6]<< v_3[7]<< v_3[8];
//  barSeries->append(set1_LS);
//  *set2_LS << v_5[0] << v_5[1]<< v_5[2]<< v_5[3]<< v_5[4]<< v_5[5]<< v_5[6]<< v_5[7]<< v_5[8];
//  barSeries->append(set2_LS);
//  *set3_LS << v_7[0] << v_7[1]<< v_7[2]<< v_7[3]<< v_7[4]<< v_7[5]<< v_7[6]<< v_7[7]<< v_7[8];
//  barSeries->append(set3_LS);
//  update();

//}

void TwoFilter::scale(int index, int slid)
{
    if (index > slid) chartView->chart()->zoomIn();
    if (index < slid) chartView->chart()->zoomOut();
    update();

}

void TwoFilter::clear()
{
    chartView->chart()->zoomReset();

}

void TwoFilter::clearGr()
{
    set0_LS->remove(0,set0_LS->count());
    barSeries->append(set0_LS);
    set1_LS->remove(0,set1_LS->count());
    barSeries->append(set1_LS);
    set2_LS->remove(0,set2_LS->count());
    barSeries->append(set2_LS);
    set3_LS->remove(0,set3_LS->count());
    barSeries->append(set3_LS);

}

void TwoFilter::moveLeft()
{
   chartView->chart()->scroll(-1,0);
}

void TwoFilter::moveRight()
{
   chartView->chart()->scroll(1,0);
}

void TwoFilter::moveUp()
{
    chartView->chart()->scroll(0,1);
}

void TwoFilter::moveDown()
{
    chartView->chart()->scroll(0,-1);
}
