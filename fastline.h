#ifndef FASTLINE_H
#define FASTLINE_H

#include <QWidget>
#include <opencv2/opencv.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

namespace Ui {
class FastLine;
}

class FastLine : public QWidget
{
    Q_OBJECT

public:
    explicit FastLine(QWidget *parent = 0);
    ~FastLine();
    cv::Mat image_fastLine;
    cv::Mat imageMid;

private:
    Ui::FastLine *ui;
signals:
    void sent_on_label (cv::Mat img);
    void setGraphic(int y);
    void sent_Data(int a, int b, int c, int d, int e);


private slots:
    void setFastLine();
    void getFrame(cv::Mat img);
    void setGraph ();
    void sentData();
};

#endif // FASTLINE_H
