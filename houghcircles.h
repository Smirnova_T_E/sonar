#ifndef HOUGHCIRCLES_H
#define HOUGHCIRCLES_H

#include <QWidget>
#include <opencv2/opencv.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

namespace Ui {
class HoughCircles;
}

class HoughCircles : public QWidget
{
    Q_OBJECT

public:
    explicit HoughCircles(QWidget *parent = 0);
    ~HoughCircles();
    cv::Mat image_HC;
    cv::Mat imageMid_HC;

private:
    Ui::HoughCircles *ui;

signals:
    void sent_on_label_HC (cv::Mat img);
    void setGraphic(int y);
    void sent_Data(int a, int b, int c, int d, int e);

private slots:
    void setHoughCircles ();
    void getFrame_HC (cv::Mat img);
    void setGraph ();
    void sentData();
};

#endif // HOUGHCIRCLES_H
