#include "analysis.h"
#include "ui_analysis.h"

Analysis::Analysis(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Analysis)
{
    x_mean = 0;
    ui->setupUi(this);
    chartView = new QChartView();
    scatterSeries = new QScatterSeries();
    chart = new QChart();
    xAxis = new QValueAxis;
    yAxis = new QValueAxis;

    hlay = new QHBoxLayout();
    hlay->addWidget(chartView);
    setLayout(hlay);

    chartView->chart()->addSeries(scatterSeries);
    chartView->chart()->setAxisX(xAxis, scatterSeries);
    chartView->chart()->setAxisY(yAxis, scatterSeries);
    chartView->setRenderHint(QPainter::Antialiasing);

    xAxis->setRange(0, 10);
    xAxis->setTickCount(11);
    yAxis->setRange(0, 10);
    yAxis->setTickCount(11);
    yAxis->setTitleText("Кол-во объектов");
 //   xAxis->setTitleText("Методы");
}

Analysis::~Analysis()
{
    delete ui;
}

void Analysis::scale(int index, int slid)
{
    if (index > slid) chartView->chart()->zoomIn();
    if (index < slid) chartView->chart()->zoomOut();
    update();

}

void Analysis::clear()
{
    chartView->chart()->zoomReset();

}

void Analysis::setXY(int y)
{
    x_mean ++;
    *scatterSeries << QPointF(x_mean, y);
}

void Analysis::moveLeft()
{
   chartView->chart()->scroll(-2,0);
}

void Analysis::moveRight()
{
    chartView->chart()->scroll(2,0);
}

void Analysis::moveUp()
{
    chartView->chart()->scroll(0,2);
}

void Analysis::moveDown()
{
    chartView->chart()->scroll(0,-2);
}

void Analysis::clearGr()
{
    scatterSeries->clear();
    x_mean = 0;
}
