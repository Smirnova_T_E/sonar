#include "rangfilter.h"
#include "ui_rangfilter.h"
#include <QDebug>
using namespace std;

RangFilter::RangFilter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RangFilter)
{

    ui->setupUi(this);
    chartView = new QChartView(this);
    barSeries = new QBarSeries(this);
    chart = new QChart();
    xAxis = new QBarCategoryAxis(this);
    yAxis = new QValueAxis(this);
    this->setMouseTracking(true);

    hlay = new QHBoxLayout(this);
    hlay->addWidget(chartView);
    setLayout(hlay);

    set0 = new QBarSet ("RF");
    set1 = new QBarSet ("RF+Gauss 3x3");
    set2 = new QBarSet ("RF+Gauss 5x5");
    set3 = new QBarSet ("RF+Gauss 7x7");

    chartView->chart()->addSeries(barSeries);
    chartView->chart()->setAnimationOptions(QChart::SeriesAnimations);

    QStringList categories;
    categories << "0" << "3" << "6" << "9" << "12" << "15" << "18" << "21" << "24";

    xAxis->append(categories);
    chartView->chart()->createDefaultAxes();
    chartView->chart()->setAxisY(yAxis, barSeries);
    chartView->chart()->setAxisX(xAxis, barSeries);
    chartView->chart()->legend()->setVisible(true);
    chartView->setRenderHint(QPainter::Antialiasing);

    yAxis->setRange(0, 100);
    yAxis->setTickCount(11);
    yAxis->setTitleText("Кол-во линий");
    xAxis->setTitleText("№ пикселя фильтра");
    update();


}

RangFilter::~RangFilter()
{
    delete ui;
}

void RangFilter::setRF(vector<int> v, int index)
{
    set0->remove(0,set0->count());
    if (index == 1) {
        *set0 << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set0);
    }
    else {
        *set0 << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set0);
    }
    update();
}

void RangFilter::setRF_G3(vector<int> v, int index)
{
    set1->remove(0,set1->count());
    if (index == 1) {
        *set1 << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set1);
    }
    else {
        *set1 << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set1);
    }
    update();
}

void RangFilter::setRF_G5(vector<int> v, int index)
{
    set2->remove(0,set2->count());
    if (index == 1) {
        *set2 << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set2);
    }
    else {
        *set2 << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set2);
    }
    update();
}

void RangFilter::setRF_G7(vector<int> v, int index)
{
    set3->remove(0,set3->count());
    if (index == 1) {
        *set3 << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
        barSeries->append(set3);
    }
    else {
        *set3 << 0 << 0<< 0<< 0<< 0<< 0<< 0<< 0<< 0;
        barSeries->append(set3);
    }
    update();
}

//void RangFilter::setXY(vector <int> v, vector <int> v_3, vector <int> v_5, vector <int> v_7)
//{
//        set0->remove(0,set0->count());
//        set1->remove(0,set1->count());
//        set2->remove(0,set2->count());
//        set3->remove(0,set3->count());

//      *set0 << v[0] << v[1]<< v[2]<< v[3]<< v[4]<< v[5]<< v[6]<< v[7]<< v[8];
//      barSeries->append(set0);
//      *set1 << v_3[0] << v_3[1]<< v_3[2]<< v_3[3]<< v_3[4]<< v_3[5]<< v_3[6]<< v_3[7]<< v_3[8];
//      barSeries->append(set1);
//      *set2 << v_5[0] << v_5[1]<< v_5[2]<< v_5[3]<< v_5[4]<< v_5[5]<< v_5[6]<< v_5[7]<< v_5[8];
//      barSeries->append(set2);
//      *set3 << v_7[0] << v_7[1]<< v_7[2]<< v_7[3]<< v_7[4]<< v_7[5]<< v_7[6]<< v_7[7]<< v_7[8];
//      barSeries->append(set3);
//      update();
//}

void RangFilter::scale(int index, int slid)
{
    if (index > slid) chartView->chart()->zoomIn();
    if (index < slid) chartView->chart()->zoomOut();
    update();

}

void RangFilter::clear()
{
    chartView->chart()->zoomReset();
}

void RangFilter::clearGr()
{
    set0->remove(0,set0->count());
    barSeries->append(set0);
    set1->remove(0,set1->count());
    barSeries->append(set1);
    set2->remove(0,set2->count());
    barSeries->append(set2);
    set3->remove(0,set3->count());
    barSeries->append(set3);
}

void RangFilter::moveLeft()
{
   chartView->chart()->scroll(-2,0);
}

void RangFilter::moveRight()
{
    chartView->chart()->scroll(2,0);
}

void RangFilter::moveUp()
{
    chartView->chart()->scroll(0,2);
}

void RangFilter::moveDown()
{
    chartView->chart()->scroll(0,-2);
}


