#include "houghline.h"
#include "ui_houghline.h"
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>

class linesFinder
{
public:
    cv::Mat img;
    double deltaRho;
    double deltaTheta;
    int minVote;
    double minLength;
    double maxGap;
    std::vector <cv::Vec4i> lines;
    int k =0;
    linesFinder(): deltaRho(1), deltaTheta(M_PI/180), minVote(10), minLength(0.), maxGap(0.) {}

    void setAccResolution (double dRho, double dTheta){
        deltaRho = dRho;
        deltaTheta = dTheta;
    }

    void setMinVote (int minv){
        minVote = minv;
    }

    void setLineLengthAndGap (double length, double gap){
        minLength = length;
        maxGap = gap;
    }

    std::vector<cv::Vec4i> findLines (cv::Mat& binary, int c){
        lines.clear();

            do
            {
              k=0;
              minVote=minVote+1;
              cv::HoughLinesP(binary,lines, deltaRho, deltaTheta, minVote, minLength, maxGap);
              std::vector<cv::Vec4i>::const_iterator abc = lines.begin();
              while (abc!=lines.end())

               {
                 k=k+1;
                 abc++;
               }
            }
            while (k > c);
        return lines;
    }

    void drawDetectedLines (cv::Mat& image, cv::Scalar color){
        color = cv::Scalar(255,255,255);
        std::vector<cv::Vec4i>::const_iterator it2 = lines.begin();
        while (it2!=lines.end())
        {
          cv::Point pt1((*it2)[0], (*it2)[1]);
          cv::Point pt2((*it2)[2], (*it2)[3]);
          cv::line (image, pt1, pt2, color, 2);
          ++it2;
        }
    }
};

HoughLine::HoughLine(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HoughLine)
{
    image_HL.release();
    ui->setupUi(this);
    connect (ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(setHoughLine()));
    connect(ui->pushButton_2, SIGNAL(clicked(bool)), this, SLOT(setGraph()));
    connect(ui->pushButton_3, SIGNAL(clicked(bool)), this, SLOT(sentData()));
}

HoughLine::~HoughLine()
{
    delete ui;
}

void HoughLine::setHoughLine()
{
    imageMid_HL.release();
    imageMid_HL = image_HL.clone();
    int a = ui->spinBox_4->value();
    int b = ui->spinBox_3->value();
    int c = ui->spinBox->value();
    int d = ui->spinBox_2->value();
    cv::Canny(imageMid_HL, imageMid_HL, a, b);
    linesFinder finder_Lines;
    finder_Lines.setLineLengthAndGap(d, 20);
    finder_Lines.setMinVote(0);
    std::vector <cv::Vec4i> lines = finder_Lines.findLines(imageMid_HL, c);
    imageMid_HL = image_HL;
    finder_Lines.drawDetectedLines(imageMid_HL, cv::Scalar(255,255,255));
    emit sent_on_label_HL(imageMid_HL);
    update();
}

void HoughLine::getFrame_HL(cv::Mat img)
{
    image_HL = img;
}

void HoughLine::setGraph()
{
    int yMean = ui->spinBox_5->value();
    emit setGraphic(yMean);
}

void HoughLine::sentData()
{
    int a = ui->spinBox_4->value();
    int b = ui->spinBox_3->value();
    int c = ui->spinBox->value();
    int d = ui->spinBox_2->value();
    int e = ui->spinBox_5->value();
    emit sent_Data(a, b, c, d, e);
}
