#ifndef BAR_H
#define BAR_H

#include <QWidget>
#include <QtCharts>

namespace Ui {
class Bar;
}

class Bar : public QWidget
{
    Q_OBJECT

public:
    explicit Bar(QWidget *parent = 0);
    ~Bar();
    QChartView * chartView;
    QBarSeries *barSeries;
    QChart *chart;
    QBarCategoryAxis *xAxis;
    QValueAxis *yAxis;
    QHBoxLayout *hlay;


private:
    Ui::Bar *ui;
public slots:
    void scale (int index, int slid);
    void clear();
};

#endif // BAR_H
