#ifndef LINESEGMENT_H
#define LINESEGMENT_H

#include <QWidget>
#include <opencv2/opencv.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <QWidget>

namespace Ui {
class LineSegment;
}

class LineSegment : public QWidget
{
    Q_OBJECT

public:
    explicit LineSegment(QWidget *parent = 0);
    ~LineSegment();
    cv::Mat image_LS;
    cv::Mat imageMid_LS;

private:
    Ui::LineSegment *ui;

signals:
    void sent_on_label_LS (cv::Mat img);
    void setGraphic(int y);
    void sent_Data(int a, int b, int c, int d, int e);

private slots:
    void setLineSegment();
    void getFrame_LS(cv::Mat img);
    void setGraph ();
    void sentData();
};

#endif // LINESEGMENT_H
