#include "mainwindow.h"
#include "fastline.h"
#include "linesegment.h"
#include "ui_mainwindow.h"
#include "ui_fastline.h"

#include "opencv2/opencv.hpp"
#include <opencv2/ximgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <cmath>
#include <math.h>
#include <QImage>
#include <iostream>

#include <QDebug>
#include <iostream>
#include <fstream>

using namespace std;


class Filter{

public:
    int window[25];

    void rangFilter (cv::Mat& frame, int number, cv::Mat& dst){
         dst = frame.clone();
         for(int y = 0; y < frame.rows; y++)
             for(int x = 0; x < frame.cols; x++)
                 dst.at<uchar>(y,x) = 0.0;

         for(int y = 2; y < frame.rows - 2; y++){
             for(int x = 2; x < frame.cols - 2; x++){

                 window[0] = frame.at<uchar>(y - 1 ,x - 1);
                 window[1] = frame.at<uchar>(y, x - 1);
                 window[2] = frame.at<uchar>(y + 1, x - 1);
                 window[3] = frame.at<uchar>(y - 1, x);
                 window[4] = frame.at<uchar>(y, x);
                 window[5] = frame.at<uchar>(y + 1, x);
                 window[6] = frame.at<uchar>(y - 1, x + 1);
                 window[7] = frame.at<uchar>(y, x + 1);
                 window[8] = frame.at<uchar>(y + 1, x + 1);
                 window[9] = frame.at<uchar>(y - 2 ,x - 2);
                 window[10] = frame.at<uchar>(y, x - 2);
                 window[11] = frame.at<uchar>(y + 2, x - 2);
                 window[12] = frame.at<uchar>(y - 2, x);
                 window[13] = frame.at<uchar>(y + 2, x);
                 window[14] = frame.at<uchar>(y - 2, x + 2);
                 window[15] = frame.at<uchar>(y, x + 2);
                 window[16] = frame.at<uchar>(y + 2, x + 2);
                 window[17] = frame.at<uchar>(y - 1 ,x - 2);
                 window[18] = frame.at<uchar>(y + 1, x - 2);
                 window[19] = frame.at<uchar>(y - 2, x - 1);
                 window[20] = frame.at<uchar>(y + 2, x - 1);
                 window[21] = frame.at<uchar>(y - 2, x + 1);
                 window[22] = frame.at<uchar>(y + 2, x + 1);
                 window[23] = frame.at<uchar>(y - 1, x + 2);
                 window[24] = frame.at<uchar>(y + 1, x + 2);

                 //insertionSort(window);
                 vector<int> window_2 (window, window+25);
                 sort(window_2.begin(), window_2.end());

                 // assign the median to centered element of the matrix
                 dst.at<uchar>(y,x) = window_2[number];
             }
         }
    }
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    lines.reserve(9);
    lines_3.reserve(9);
    lines_5.reserve(9);
    lines_7.reserve(9);
    lines.clear();
    lines_3.clear();
    lines_5.clear();
    lines_7.clear();
    frame.release();
    slider = 0;

    ui->setupUi(this);
   // this->grabKeyboard();

    connect(ui->comboBox_2,SIGNAL(currentIndexChanged(int)), this, SLOT(SearchMethod(int)));
    connect(this, SIGNAL(setPanelMode(QWidget*)), ui->stackedWidget, SLOT(setCurrentWidget(QWidget*)));
    connect(ui->checkBox, SIGNAL(clicked(bool)), this, SLOT(setFilter(bool)));
    connect(ui->checkBox_2, SIGNAL(clicked(bool)), this, SLOT(setFilter_2(bool)));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setImage(int)));
    connect(ui->checkBox_3, SIGNAL(clicked(bool)), this, SLOT(setDilate(bool)));
    connect(ui->spinBox_3, SIGNAL(editingFinished()), this, SLOT(setRangFilter()));
    connect(ui->spinBox_4, SIGNAL(valueChanged(int)), this, SLOT(setGaussFilter(int)));
    connect(this, SIGNAL(sentFrame(cv::Mat)), ui->page_5, SLOT(getFrame(cv::Mat)));
    connect(ui->page_5, SIGNAL(sent_on_label(cv::Mat)), this, SLOT(set_on_label(cv::Mat)));
    connect(this, SIGNAL(setInitialFrame(cv::Mat)), this, SLOT(set_Initial_Frame(cv::Mat)));
    connect(this, SIGNAL(sentFrame_LS(cv::Mat)), ui->page_4, SLOT(getFrame_LS(cv::Mat)));
    connect(ui->page_4, SIGNAL(sent_on_label_LS(cv::Mat)), this, SLOT(set_on_label(cv::Mat)));
    connect(this, SIGNAL(sentFrame_HL(cv::Mat)), ui->page_2, SLOT(getFrame_HL(cv::Mat)));
    connect(ui->page_2, SIGNAL(sent_on_label_HL(cv::Mat)), this, SLOT(set_on_label_HL(cv::Mat)));
    connect(this, SIGNAL(sentFrame_HC(cv::Mat)), ui->page_3, SLOT(getFrame_HC(cv::Mat)));
    connect(ui->page_3, SIGNAL(sent_on_label_HC(cv::Mat)), this, SLOT(set_on_label_HL(cv::Mat)));

    connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(setDilateOnFrame(int)));

    connect(ui->comboBox_4, SIGNAL(currentIndexChanged(int)), this, SLOT(setGraphic(int)));
    connect(this, SIGNAL(setGraph(QWidget*)), ui->stackedWidget_2, SLOT(setCurrentWidget(QWidget*)));
    connect (this, SIGNAL(graph_rangFilter()), this, SLOT(graphRangFilter()));
    connect(this, SIGNAL(graph_LineSegment()), this, SLOT(graphLineSegment2()));

    connect (ui->horizontalSlider_2, SIGNAL(sliderPressed()), this, SLOT (slider_int()));
    connect (ui->horizontalSlider_2, SIGNAL(valueChanged(int)), this, SLOT(setScale(int)));
    connect(this, SIGNAL(set0(int, int)), ui->page_6,SLOT(scale(int, int)));
    connect(this, SIGNAL(set1(int, int)), ui->page_7,SLOT(scale(int, int)));
    connect(this, SIGNAL(set2(int, int)), ui->page_8,SLOT(scale(int, int)));
    connect(this, SIGNAL(set3(int, int)), ui->page_9,SLOT(scale(int, int)));

    connect(ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(clearScale()));
    connect(this, SIGNAL(clear0()), ui->page_6,SLOT(clear()));
    connect(this, SIGNAL(clear1()), ui->page_7,SLOT(clear()));
    connect(this, SIGNAL(clear2()), ui->page_8,SLOT(clear()));
    connect(this, SIGNAL(clear3()), ui->page_9,SLOT(clear()));

    connect(ui->checkBox_4, SIGNAL(clicked(bool)), this, SLOT(setRF(bool)));
    connect(this, SIGNAL(RF0(vector<int>,int)), ui->page_7, SLOT(setRF(vector<int>,int)));
    connect(this, SIGNAL(LS0(vector<int>,int)), ui->page_8, SLOT(setRF(vector<int>,int)));
    connect(ui->checkBox_5, SIGNAL(clicked(bool)), this, SLOT(setRF_G3(bool)));
    connect(this, SIGNAL(RF1(vector<int>,int)), ui->page_7, SLOT(setRF_G3(vector<int>,int)));
    connect(this, SIGNAL(LS1(vector<int>,int)), ui->page_8, SLOT(setRF_G3(vector<int>,int)));
    connect(ui->checkBox_6, SIGNAL(clicked(bool)), this, SLOT(setRF_G5(bool)));
    connect(this, SIGNAL(RF2(vector<int>,int)), ui->page_7, SLOT(setRF_G5(vector<int>,int)));
    connect(this, SIGNAL(LS2(vector<int>,int)), ui->page_8, SLOT(setRF_G5(vector<int>,int)));
    connect(ui->checkBox_7, SIGNAL(clicked(bool)), this, SLOT(setRF_G7(bool)));
    connect(this, SIGNAL(RF3(vector<int>,int)), ui->page_7, SLOT(setRF_G7(vector<int>,int)));
    connect(this, SIGNAL(LS3(vector<int>,int)), ui->page_8, SLOT(setRF_G7(vector<int>,int)));

    connect(this, SIGNAL(clearGr()), ui->page_7, SLOT(clearGr()));
    connect(this, SIGNAL(clearGr2()), ui->page_8, SLOT(clearGr()));

    connect(this, SIGNAL(right_RF()), ui->page_7, SLOT(moveRight()));
    connect(this, SIGNAL(left_RF()), ui->page_7, SLOT(moveLeft()));
    connect(this, SIGNAL(up_RF()), ui->page_7, SLOT(moveUp()));
    connect(this, SIGNAL(down_RF()), ui->page_7, SLOT(moveDown()));

    connect(this, SIGNAL(right_two()), ui->page_8, SLOT(moveRight()));
    connect(this, SIGNAL(left_two()), ui->page_8, SLOT(moveLeft()));
    connect(this, SIGNAL(up_two()), ui->page_8, SLOT(moveUp()));
    connect(this, SIGNAL(down_two()), ui->page_8, SLOT(moveDown()));

    connect(ui->page_5, SIGNAL(setGraphic(int)), ui->page_9, SLOT(setXY(int)));
    connect (ui->page_5, SIGNAL(setGraphic(int)), this, SLOT(setGraphMethod()));
    connect(this, SIGNAL(set_Graphmethod(QWidget*)),ui->stackedWidget_2, SLOT(setCurrentWidget(QWidget*)));

    connect(this, SIGNAL(right_An()), ui->page_9, SLOT(moveRight()));
    connect(this, SIGNAL(left_An()), ui->page_9, SLOT(moveLeft()));
    connect(this, SIGNAL(up_An()), ui->page_9, SLOT(moveUp()));
    connect(this, SIGNAL(down_An()), ui->page_9, SLOT(moveDown()));
    connect(ui->comboBox_2, SIGNAL(currentIndexChanged(int)), ui->page_9, SLOT(clearGr()));

    connect(ui->page_4, SIGNAL(setGraphic(int)), ui->page_9, SLOT(setXY(int)));
    connect(ui->page_4, SIGNAL(setGraphic(int)), this, SLOT(setGraphMethod()));
    connect(ui->page_3, SIGNAL(setGraphic(int)), ui->page_9, SLOT(setXY(int)));
    connect(ui->page_3, SIGNAL(setGraphic(int)), this, SLOT(setGraphMethod()));
    connect(ui->page_2, SIGNAL(setGraphic(int)), ui->page_9, SLOT(setXY(int)));
    connect(ui->page_2, SIGNAL(setGraphic(int)), this, SLOT(setGraphMethod()));

    connect(ui->page_5, SIGNAL(sent_Data(int,int,int,int,int)), this, SLOT(writeFile(int,int,int,int,int)));
    connect(ui->page_4, SIGNAL(sent_Data(int,int,int,int,int)), this, SLOT(writeFile(int,int,int,int,int)));
    connect(ui->page_3, SIGNAL(sent_Data(int,int,int,int,int)), this, SLOT(writeFile(int,int,int,int,int)));
    connect(ui->page_2, SIGNAL(sent_Data(int,int,int,int,int)), this, SLOT(writeFile(int,int,int,int,int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *e)
{
    centralWidget()->setFocus();
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
        case Qt::Key_Left:
        if (ui->stackedWidget_2->currentWidget() == ui->page_7) emit left_RF();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_8) emit left_two();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_9) emit left_An();
            break;
        case Qt::Key_Right:
        if (ui->stackedWidget_2->currentWidget() == ui->page_7) emit right_RF();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_8) emit right_two();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_9) emit right_An();
            break;
        case Qt::Key_Up:
        if (ui->stackedWidget_2->currentWidget() == ui->page_7) emit up_RF();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_8) emit up_two();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_9) emit up_An();
            break;
        case Qt::Key_Down:
        if (ui->stackedWidget_2->currentWidget() == ui->page_7) emit down_RF();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_8) emit down_two();
        else if (ui->stackedWidget_2->currentWidget() == ui->page_9) emit down_An();
            break;
    }
}


void MainWindow::SearchMethod(int index)
{
  if (ui->comboBox->currentIndex() != 0){
  if (index == 0) {
      emit setPanelMode(ui->page);
      emit setInitialFrame(frame);
  }
  if (index == 1) {
      emit setPanelMode(ui->page_2);
      emit sentFrame_HL(frame);
  }
  if (index == 2) {
      emit setPanelMode(ui->page_3);
      emit sentFrame_HC(frame);
  }
  if (index == 3) {
      emit setPanelMode(ui->page_5);
      emit sentFrame(frame);
  }
  if (index == 4) {
      emit setPanelMode(ui->page_4);
      emit sentFrame_LS(frame);
  }
  }
  update();
}

void MainWindow::setFilter(bool checked)
{
    if (checked) ui->spinBox_3->setEnabled(true);
    else {

        ui->spinBox_3->setEnabled(false);
        ui->spinBox_3->setValue(0);
        if (ui->checkBox_2->checkState()){
            image_1 =  image_2;
            setGaussFilter(ui->spinBox_4->value());
        }
        else setImage(ui->comboBox->currentIndex());
    }
}

void MainWindow::setFilter_2(bool checked)
{
    if (checked) ui->spinBox_4->setEnabled(true);
    else {
        ui->spinBox_4->setEnabled(false);
        ui->spinBox_4->setValue(3);
        if (ui->checkBox->checkState()) {

            image =  image_2;
        //    setRangFilter(ui->spinBox_3->value());
            setRangFilter();
        }
        else setImage(ui->comboBox->currentIndex());
    }

}

void MainWindow::setImage(int index)
{
    QImage img;
    if (index == 0) {
        ui->label_2->clear();
        update();
    }
    if (index == 1) {
        QPixmap pix_1 (":/gbo111.jpg");
        img = pix_1.toImage();
      //  pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
      //  ui->label_2->setPixmap(pix);
      //  ui->label_2->setAlignment(Qt::AlignCenter);
        frame = cv::Mat(img.height(), img.width(),CV_8UC4,img.bits(),img.bytesPerLine());
        cv::cvtColor(frame,frame, CV_BGR2GRAY);
        QPixmap pix;
        pix = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows,QImage::Format_Grayscale8));
        pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
        ui->label_2->setPixmap(pix);
        ui->label_2->setAlignment(Qt::AlignCenter);
        update();
    }
    if (index == 2) {
        QPixmap pix_1 (":/gbo22.jpg");
        img = pix_1.toImage();
        frame = cv::Mat(img.height(), img.width(),CV_8UC4,img.bits(),img.bytesPerLine());
        cv::cvtColor(frame,frame, CV_BGR2GRAY);
        QPixmap pix;
        pix = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows,QImage::Format_Grayscale8));
        pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
        ui->label_2->setPixmap(pix);
        ui->label_2->setAlignment(Qt::AlignCenter);
        update();
    }
    if (index == 3) {
        QPixmap pix_1 (":/1234[4491].jpg");
        img = pix_1.toImage();
        frame = cv::Mat(img.height(), img.width(),CV_8UC4,img.bits(),img.bytesPerLine());
        cv::cvtColor(frame,frame, CV_BGR2GRAY);
        QPixmap pix;
        pix = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows,QImage::Format_Grayscale8));
        pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
        ui->label_2->setPixmap(pix);
        ui->label_2->setAlignment(Qt::AlignCenter);
        update();
    }
    image = frame;
    image_1 = frame;
    image_2 = frame;
    image_3 = frame;

}

void MainWindow::setDilate(bool checked)
{
    if (checked) {
        ui->label_5->setEnabled(true);
        ui->label_14->setEnabled(true);
        ui->horizontalSlider->setEnabled(true);
        ui->comboBox_3->setEnabled(true);
        image_3 = frame;
    }
    else {
        ui->label_5->setEnabled(false);
        ui->label_14->setEnabled(false);
        ui->horizontalSlider->setEnabled(false);
        ui->horizontalSlider->setValue(0);
        ui->comboBox_3->setEnabled(false);
        frame = image_3;
        set_Initial_Frame(frame);
    }

}

void MainWindow::setRangFilter()
{
    frame = image;
    cv::Mat frame_1;
    int index = ui->spinBox_3->value();
    Filter filter_image;
  //  qDebug() << index << "before";
    filter_image.rangFilter(frame, index, frame_1);
  //  qDebug() << index << "after";
    frame = frame_1.clone();
    QPixmap pix;
    pix = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows,QImage::Format_Grayscale8));
    pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
    image_1 = frame;
    update();

}

void MainWindow::setGaussFilter(int index)
{
    frame = image_1;
    cv::GaussianBlur(frame, frame, cv::Size(index,index), 1.5);
    QPixmap pix;
    pix = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows, QImage::Format_Grayscale8));
    pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
    image = frame;
    update();
}

void MainWindow::set_on_label(cv::Mat img)
{
  //  cv::imwrite("../filter10.jpg", img);
    cv::cvtColor(img, img, CV_BGR2RGB);
    QPixmap pix;
    pix = QPixmap::fromImage(QImage(img.data, img.cols, img.rows, QImage::Format_RGB888));
    pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
    update();

}

void MainWindow::set_on_label_HL(cv::Mat img)
{
    ui->label_2->clear();
    QPixmap pix;
    pix = QPixmap::fromImage(QImage(img.data, img.cols, img.rows, QImage::Format_Grayscale8));
    pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
    img.release();
    update();

}

void MainWindow::set_Initial_Frame(cv::Mat img)
{
    img = frame.clone();
    QPixmap pix;
    pix = QPixmap::fromImage(QImage(img.data, img.cols, img.rows, QImage::Format_Grayscale8));
    pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
    update();

}

void MainWindow::setDilateOnFrame(int index)
{
    if (ui->comboBox->currentIndex() != 0){
    frame = image_3;
    cv::Mat frame_1;
    int dilation_size = index;
    int c = 0;
    if (ui->comboBox_3->currentIndex() == 0) c = cv::MORPH_RECT;
    if (ui->comboBox_3->currentIndex() == 1) c = cv::MORPH_ELLIPSE;
    if (ui->comboBox_3->currentIndex() == 2) c = cv::MORPH_CROSS;
    cv::Mat element = cv::getStructuringElement(c, cv::Size(2*dilation_size + 1, 2*dilation_size+1),
                      cv::Point(dilation_size, dilation_size));
    cv::erode(frame, frame_1, element);
    cv::dilate(frame_1, frame_1, element);
    frame = frame_1.clone();
    QPixmap pix;
    pix = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows,QImage::Format_Grayscale8));
    pix = pix.scaled(QSize(810,550),Qt::KeepAspectRatio);
    ui->label_2->setPixmap(pix);
    ui->label_2->setAlignment(Qt::AlignCenter);
    }
    update();

}

void MainWindow::setGraphic(int index)
{
    ui->checkBox_4->setChecked(false);
    ui->checkBox_5->setChecked(false);
    ui->checkBox_6->setChecked(false);
    ui->checkBox_7->setChecked(false);
    if (index == 0) {
        emit setGraph(ui->page_6);
    }
    if (index == 1) {
        emit setGraph(ui->page_7);
        emit graph_rangFilter();
        emit clearGr();
    }
    if (index == 2) {
       emit setGraph(ui->page_8);
       emit graph_LineSegment();
       emit clearGr2();
    }
    update();
}

void MainWindow::graphRangFilter()
{
    std::vector <int> RFlines (9), RFlines_3 (9), RFlines_5 (9),RFlines_7 (9);
    cv::Mat forFilter, forFilter_3, forFilter_5, forFilter_7;
    forFilter.release();
    forFilter_3.release();
    forFilter_5.release();
    forFilter_7.release();
    if (ui->comboBox->currentIndex() != 0){
    image_rangFilter = image_2;
    int c = 0;
    for (int i = 0; i <= 24; i = i + 3){
        Filter rangFilter_2;
        rangFilter_2.rangFilter(image_rangFilter, i, forFilter);
        cv::GaussianBlur(forFilter, forFilter_3, cv::Size(3,3), 1.5);
        cv::GaussianBlur(forFilter, forFilter_5, cv::Size(5,5), 1.5);
        cv::GaussianBlur(forFilter, forFilter_7, cv::Size(7,7), 1.5);

        cv::Ptr <cv::ximgproc::FastLineDetector> fastLine = cv::ximgproc::createFastLineDetector(40, 3.0, 50, 50, 7, true);
        std::vector <cv::Vec4f> lines_fld;
        std::vector <cv::Vec4f> lines_fld_3;
        std::vector <cv::Vec4f> lines_fld_5;
        std::vector <cv::Vec4f> lines_fld_7;
        fastLine->detect(forFilter, lines_fld);
        fastLine->detect(forFilter_3, lines_fld_3);
        fastLine->detect(forFilter_5, lines_fld_5);
        fastLine->detect(forFilter_7, lines_fld_7);
        std::vector<cv::Vec4f>::const_iterator abc = lines_fld.begin();
        int k = 0;
        while (abc!=lines_fld.end())
             {
               k=k+1; abc++;
             }
        RFlines[c] = k;
        k = 0;
        abc = lines_fld_3.begin();
        while (abc!=lines_fld_3.end())
             {
               k=k+1; abc++;
             }
        RFlines_3[c] = k;
        k = 0;
        abc = lines_fld_5.begin();
        while (abc!=lines_fld_5.end())
             {
               k=k+1; abc++;
             }
        RFlines_5[c] = k;
        k = 0;
        abc = lines_fld_7.begin();
        while (abc!=lines_fld_7.end())
             {
               k=k+1; abc++;
             }
        RFlines_7[c] = k;
        c ++;
    }
    lines = RFlines;
    lines_3 = RFlines_3;
    lines_5 = RFlines_5;
    lines_7 = RFlines_7;
  //  emit setXY(RFlines, RFlines_3, RFlines_5, RFlines_7);
    update();
    }
}

void MainWindow::graphLineSegment2()
{
    std::vector <int> LSlines (9), LSlines_3 (9), LSlines_5 (9),LSlines_7 (9);
    cv::Mat forFilter, forFilter_3, forFilter_5, forFilter_7;
    forFilter.release();
    forFilter_3.release();
    forFilter_5.release();
    forFilter_7.release();
    if (ui->comboBox->currentIndex() != 0){
    image_LineSegment = image_2;
    int c = 0;
    for (int i = 0; i <= 24; i = i + 3){
        Filter rangFilter_2;
        rangFilter_2.rangFilter(image_LineSegment, i, forFilter);
        cv::GaussianBlur(forFilter, forFilter_3, cv::Size(3,3), 1.5);
        cv::GaussianBlur(forFilter, forFilter_5, cv::Size(5,5), 1.5);
        cv::GaussianBlur(forFilter, forFilter_7, cv::Size(7,7), 1.5);
        cv::Canny(forFilter, forFilter, 50, 200,3);
        cv::Canny(forFilter_3, forFilter_3, 50, 200,3);
        cv::Canny(forFilter_5, forFilter_5, 50, 200,3);
        cv::Canny(forFilter_7, forFilter_7, 50, 200,3);
        cv::Ptr <cv::LineSegmentDetector> ls = cv::createLineSegmentDetector();
        std::vector <cv::Vec4f> lines_fld;
        std::vector <cv::Vec4f> lines_fld_3;
        std::vector <cv::Vec4f> lines_fld_5;
        std::vector <cv::Vec4f> lines_fld_7;
        ls->detect(forFilter, lines_fld);
        ls->detect(forFilter_3, lines_fld_3);
        ls->detect(forFilter_5, lines_fld_5);
        ls->detect(forFilter_7, lines_fld_7);
        std::vector<cv::Vec4f>::const_iterator abc = lines_fld.begin();
        int k = 0;
        while (abc!=lines_fld.end())
             {
               k=k+1; abc++;
             }
        LSlines[c] = k;
        k = 0;
        abc = lines_fld_3.begin();
        while (abc!=lines_fld_3.end())
             {
               k=k+1; abc++;
             }
        LSlines_3[c] = k;
        k = 0;
        abc = lines_fld_5.begin();
        while (abc!=lines_fld_5.end())
             {
               k=k+1; abc++;
             }
        LSlines_5[c] = k;
        k = 0;
        abc = lines_fld_7.begin();
        while (abc!=lines_fld_7.end())
             {
               k=k+1; abc++;
             }
        LSlines_7[c] = k;
        c ++;
    }
    lines = LSlines;
    lines_3 = LSlines_3;
    lines_5 = LSlines_5;
    lines_7 = LSlines_7;
    //emit setXY_LS(LSlines, LSlines_3, LSlines_5, LSlines_7);
    }
    update();
}

void MainWindow::setScale(int index)
{
    int a = index;
    if (ui->stackedWidget_2->currentWidget() == ui->page_6) emit set0(a, slider);
    if (ui->stackedWidget_2->currentWidget() == ui->page_7) emit set1(a, slider);
    if (ui->stackedWidget_2->currentWidget() == ui->page_8) emit set2(a, slider);
    if (ui->stackedWidget_2->currentWidget() == ui->page_9) emit set3(a, slider);
}

void MainWindow::slider_int()
{
    slider = ui->horizontalSlider_2->value();

}

void MainWindow::clearScale()
{
    ui->horizontalSlider_2->setValue(0);
    if (ui->stackedWidget_2->currentWidget() == ui->page_6) emit clear0();
    if (ui->stackedWidget_2->currentWidget() == ui->page_7) emit clear1();
    if (ui->stackedWidget_2->currentWidget() == ui->page_8) emit clear2();
    if (ui->stackedWidget_2->currentWidget() == ui->page_9) emit clear3();
}

void MainWindow::setRF(bool checked)
{
    if (ui->comboBox->currentIndex() != 0){
    if (ui->comboBox_4->currentIndex() == 1) {
        if (checked) emit RF0(lines, 1);
        else emit RF0(lines, 0);
    }
    else if (ui->comboBox_4->currentIndex() == 2) {
        if (checked) emit LS0(lines, 1);
        else emit LS0(lines, 0);
    }
    }
    update();
}

void MainWindow::setRF_G3(bool checked)
{
    if (ui->comboBox->currentIndex() != 0){
    if (ui->comboBox_4->currentIndex() == 1) {
        if (checked) emit RF1(lines_3, 1);
        else emit RF1(lines_3, 0);
    }
    else if (ui->comboBox_4->currentIndex() == 2) {
        if (checked) emit LS1(lines_3, 1);
        else emit LS1(lines_3, 0);
    }
    }
    update();
}

void MainWindow::setRF_G5(bool checked)
{
    if (ui->comboBox->currentIndex() != 0){
    if (ui->comboBox_4->currentIndex() == 1) {
        if (checked) emit RF2(lines_5, 1);
        else emit RF2(lines_5, 0);
    }
    else if (ui->comboBox_4->currentIndex() == 2) {
        if (checked) emit LS2(lines_5, 1);
        else emit LS2(lines_5, 0);
    }
    }
    update();
}

void MainWindow::setRF_G7(bool checked)
{
    if (ui->comboBox->currentIndex() != 0){
    if (ui->comboBox_4->currentIndex() == 1) {
        if (checked) emit RF3(lines_7, 1);
        else emit RF3(lines_7, 0);
    }
    else if (ui->comboBox_4->currentIndex() == 2) {
        if (checked) emit LS3(lines_7, 1);
        else emit LS3(lines_7, 0);
    }
    }
    update();
}

void MainWindow::setGraphMethod()
{
    emit set_Graphmethod(ui->page_9);
}

void MainWindow::writeFile(int a, int b, int c, int d, int e)
{
    ofstream out ("../sonarData.txt", ios::app);
  //  out.open("../sonarData.txt");
    if (out.is_open()) {
        if (ui->stackedWidget->currentWidget() == ui->page_2){
            out << "Hough Line Detector"<< endl;
            out << "Кол-во линий =" << c << endl;
            out << "Min длина линий =" << d << endl;
            out << "Верхний порог Canny =" << b << endl;
            out << "Нижний порог Canny =" << a << endl;
            out << "Кол-во объектов =" << e << endl;
            out <<endl;
        }
        if (ui->stackedWidget->currentWidget() == ui->page_3){
            out << "Hough Circles Detector"<< endl;
            out << "Кол-во окружностей =" << c << endl;
            out << "Min радиус =" << b << endl;
            out << "Max радиус =" << a << endl;
            out << "Верхний порог Canny =" << d << endl;
            out << "Кол-во объектов =" << e << endl;
            out <<endl;
        }
        if (ui->stackedWidget->currentWidget() == ui->page_4){
            out << "Line Segment Detector"<< endl;
            out << "Верхний порог Canny =" << a << endl;
            out << "Нижний порог Canny =" << b << endl;
            out << "Оператор Собеля =" << c << endl;
            out << "Кол-во объектов =" << d << endl;
            out <<endl;
        }
        if (ui->stackedWidget->currentWidget() == ui->page_5){
            out << "Fast Line Detector"<< endl;
            out << "Min длина линий =" << a << endl;
            out << "Верхний порог Canny =" << b << endl;
            out << "Нижний порог Canny =" << c << endl;
            out << "Оператор Собеля =" << d << endl;
            out << "Кол-во объектов =" << e << endl;
            out <<endl;
        }
    }
}
